﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public interface IWebHostBuilder
    {
        IWebHostBuilder UseServer(IServer server);

        IWebHostBuilder Configure(Action<IApplicationBuilder> configure);

        IWebHost Build();
    }

    public class WebHostBuilder : IWebHostBuilder
    {
        IServer _server;
        List<Action<IApplicationBuilder>> _configurs = new List<Action<IApplicationBuilder>>();

        public IWebHost Build()
        {
            var builder = new ApplicationBuilder();
            foreach (var app in _configurs)
            {
                app(builder);
            }

            return new WebHost(this._server, builder.Build());
        }

        public IWebHostBuilder Configure(Action<IApplicationBuilder> configure)
        {
            _configurs.Add(configure);
            return this;
        }

        public IWebHostBuilder UseServer(IServer server)
        {
            _server = server;
            return this;
        }
    }
}
