﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class HttpContext
    {
        public HttpRequest Request {  get; set; }
        public HttpResponse Response { get; set; }
    }

    public class HttpRequest
    {
        public Uri Url { get; set; }
        public NameValueCollection Headers{ get; set; }
        public Stream Body { get; set; }
    }

    public class HttpResponse
    {
        public NameValueCollection Headers { get; set; }
        public Stream Body { get; set; }

        public int StatusCode { get; set; }

        public Task WriteAsync(string contents)
        {
            var buffer = Encoding.UTF8.GetBytes(contents);
            return Body.WriteAsync(buffer, 0, buffer.Length);
        }
    }
}
