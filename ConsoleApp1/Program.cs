﻿using System.Net;

namespace ConsoleApp1
{
    internal class Program
    {
        public static async Task Main()
        {
            await new WebHostBuilder()
                .UseServer(new HttpListenerServer(new string[] { "http://localhost:5001/" }))
                .Configure(app => app.Use(AMiddleware))
                .Configure(app => app.Use(BMiddleware))
                .Configure(app => app.Use(CMiddleware))
                .Build().StartAsync();
        }


        public static RequestDelegate AMiddleware(RequestDelegate next)
        => async context =>
        {
            await context.Response.WriteAsync("A=>");
            await next(context);
        };

        public static RequestDelegate BMiddleware(RequestDelegate next)
       => async context =>
       {
           await context.Response.WriteAsync("B=>");
           await next(context);
       };

        public static RequestDelegate CMiddleware(RequestDelegate next)
       => async context =>
       {
           await context.Response.WriteAsync("C");
           await next(context);
       };
    }
}